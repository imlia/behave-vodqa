from selenium import webdriver

class Browser(object):

    driver = webdriver.Chrome('features/node_modules/.bin/chromedriver')
    driver.implicitly_wait(30)
    driver.set_page_load_timeout(30)
    driver.maximize_window()

#setting up with chrome options
    def chrome_driver(self, driver_config):
        chrome_options = webdriver.ChromeOptions()
        [chrome_options.add_argument('--{}'.format(driver_option)) for driver_option in driver_config['options']]
        driver = webdriver.Chrome(executable_path='features/node_modules/.bin/chromedriver', chrome_options=chrome_options)
        driver.maximize_window()
        return driver
#running with phantom prebuilt
    def phantom_js_driver(self):
        driver = webdriver.PhantomJS('features/node_modules/.bin/phantomjs', service_args=['--ignore-ssl-errors=true'])
        driver.set_window_size(1920, 1080)
        return driver

    def close(context):
        context.driver.close()
