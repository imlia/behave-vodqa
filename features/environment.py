import os
import time
from selenium import webdriver
from browser import Browser
from pages.home_page import HomePage
from pages.search_results_page import SearchResultsPage
from behave.log_capture import capture


def before_all(context):
    context.browser = Browser()
    context.home_page = HomePage()
    context.search_results_page = SearchResultsPage()

def after_all(context):
    context.browser.close()

@capture
def after_scenario(context, scenario):
    print("scenario status " + scenario.status)
    if scenario.status == 'failed':
        scenario_error_dir = 'feature_errors'
        if not os.path.exists(scenario_error_dir):
            os.mkdir(scenario_error_dir)
        scenario_file_path = os.path.join(scenario_error_dir, scenario.feature.name.replace(' ', '_')
                                          + '_' + time.strftime("%H%M%S_%d_%m_%Y")
                                          + '.jpg')
        context.browser.driver.save_screenshot(scenario_file_path)